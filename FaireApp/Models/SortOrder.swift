//
//  SortOrder.swift
//  FaireApp
//
//  Created by Alan Henrique Pégoli on 23/03/19.
//  Copyright © 2019 Faire. All rights reserved.
//

import Foundation

enum SortOrder: String, Codable {
    case asc = "ASC"
    case desc = "DESC"
}
