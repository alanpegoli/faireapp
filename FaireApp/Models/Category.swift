//
//  Category.swift
//  FaireApp
//
//  Created by Alan Henrique Pégoli on 21/03/19.
//  Copyright © 2019 Faire. All rights reserved.
//

import Foundation

struct Category: Codable {
    var name: String
    var subCategories: [Category]
    var isProductBased: Bool? = false
    var isFeatured: Bool? = false
    
    var isCategoryFeatured: Bool {
        return isFeatured ?? false
    }
}
