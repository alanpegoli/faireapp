//
//  ProductsViewController.swift
//  FaireApp
//
//  Created by Alan Henrique Pégoli on 22/03/19.
//  Copyright © 2019 Faire. All rights reserved.
//

import UIKit

class ProductsViewController: UICollectionViewController {
    
    private let refreshControl = UIRefreshControl()
    private let cellIdentifier = "ProductCollectionViewCell"
    private let connectionManager = ConnectionManager()
    
    var products: [Product] = []
    var brandToken: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib.init(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        refreshControl.addTarget(self, action: #selector(getProducts), for: .valueChanged)
        collectionView.addSubview(refreshControl)
        collectionView.alwaysBounceVertical = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if products.isEmpty { getProducts() }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
        guard let productCell = cell as? ProductCollectionViewCell else { return cell }
        let product = products[indexPath.item]
        productCell.setUp(with: product)
        return productCell
    }
    
    @objc private func getProducts() {
        guard products.isEmpty else { return }
        guard let brandToken = brandToken else { return }
        refreshControl.beginRefreshing()
        connectionManager.getMakerProducts(brandToken: brandToken) { [weak self] (result) in
            guard let strongSelf = self else { return }
            switch result {
            case let .success(products):
                strongSelf.products = products
                print(products.count)
                DispatchQueue.main.async {
                    strongSelf.collectionView.reloadData()
                }
            case let .error(error):
                strongSelf.showError(error)
            }
            DispatchQueue.main.async {
                strongSelf.refreshControl.endRefreshing()
            }
        }
    }
    
    private func showError(_ error: Error) {
        let alert = UIAlertController(title: "Something Went Wrong", message: error.localizedDescription, preferredStyle: .alert)
        let retry = UIAlertAction(title: "Retry", style: .default, handler: { _ in
            self.getProducts()
        })
        alert.addAction(retry)
        present(alert, animated: true, completion: nil)
    }
}

extension ProductsViewController: UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfCellsForRow = 1
        let spaceInBetween: CGFloat = 16 * (CGFloat(numberOfCellsForRow) + 1)
        let width = (collectionView.bounds.width-spaceInBetween)/CGFloat(numberOfCellsForRow)
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
}
