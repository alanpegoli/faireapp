//
//  Brand.swift
//  FaireApp
//
//  Created by Alan Henrique Pégoli on 23/03/19.
//  Copyright © 2019 Faire. All rights reserved.
//

import Foundation

struct Brand: Codable {
    var token: String
    var name: String
    var minimumOrderAmountCents: Int
    var images: [URLImage]
    
    var minimumOrderAmountCentsString: String {
        return "$ \(minimumOrderAmountCents) minimum"
    }
}
