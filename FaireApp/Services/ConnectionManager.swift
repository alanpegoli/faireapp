//
//  ConnectionManager.swift
//  FaireApp
//
//  Created by Alan Henrique Pégoli on 21/03/19.
//  Copyright © 2019 Faire. All rights reserved.
//

import UIKit

class ConnectionManager {
    
    enum Router {
        case getCategories
        case searchMakers(filter: SearchMakersWithFiltersRequest)
        case getMakerProducts(brandToken: String)
        
        var url: URL? {
            switch self {
            case .getCategories:
                return URL(string: "https://www.faire.com/api/category/new")
            case .searchMakers:
                return URL(string: "https://www.faire.com/api/search/makers-with-filters")
            case let .getMakerProducts(brandToken):
                return URL(string: "https://www.faire.com/api/brand/\(brandToken)/products")
            }
        }
        
        var httpMethod: String {
            if case .searchMakers = self {
                return "POST"
            }
            return "GET"
        }
        
        var httpBody: Data? {
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            if case let .searchMakers(filter) = self {
                return try? encoder.encode(filter)
            }
            return nil
        }
        
    }
    
    enum Result<Element, Error> {
        case success(Element)
        case error(Error)
    }
    
    func requestJSON<Element: Decodable>(from path: Router, completion: @escaping (_ result: Result<Element, Error>) -> Void) {
        guard let url = path.url else { return }
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = path.httpMethod
        request.httpBody = path.httpBody
        let session = URLSession(configuration: .default)
        let dataTask = session.dataTask(with: request) { (data, _, error) in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            if let error = error {
                completion(.error(error))
            }
            if let data = data {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                do {
                    let element = try decoder.decode(Element.self, from: data)
                    completion(.success(element))
                } catch {
                    completion(.error(error))
                }
            }
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        dataTask.resume()
    }
    
    func getCategories(_ completion: @escaping (_ result: Result<[Category], Error>) -> Void) {
        requestJSON(from: .getCategories) { (result: Result<[Category], Error>) in
            switch result {
            case let .success(categories):
                completion(.success(categories))
            case let .error(error):
                completion(.error(error))
            }
        }
    }
    
    func getMakerProducts(brandToken: String, _ completion: @escaping (_ result: Result<[Product], Error>) -> Void) {
        requestJSON(from: .getMakerProducts(brandToken: brandToken)) { (result: Result<[Product], Error>) in
            switch result {
            case let .success(products):
                completion(.success(products))
            case let .error(error):
                completion(.error(error))
            }
        }
    }
    
    func searchMakers(by category: Category, pageNumber: Int = 0, _ completion: @escaping (_ result: Result<[Brand], Error>) -> Void)  {
        let filter = SearchMakersWithFiltersRequest(category: category.name, paginationData: nil, sortOrder: .desc, sortBy: .name)
        requestJSON(from: .searchMakers(filter: filter)) { (result: Result<SearchMakersWithFiltersResponse, Error>) in
            switch result {
            case let .success(filtersResponse):
                completion(.success(filtersResponse.brands))
            case let .error(error):
                completion(.error(error))
            }
        }
    }
}
