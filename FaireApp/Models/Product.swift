//
//  Product.swift
//  FaireApp
//
//  Created by Alan Henrique Pégoli on 22/03/19.
//  Copyright © 2019 Faire. All rights reserved.
//

import Foundation

struct Product: Codable {
    var name: String
    var shortDescription: String
    var description: String
    var wholesalePriceCents: Int
    var retailPriceCents: Int
    var unitMultiplier: Int
    var categories: [String]
    var images: [URLImage]
    var isNew: Bool
    
    var categoriesString: String {
        var string = ""
        for (index, category) in categories.enumerated() {
            if index == categories.count - 1 {
                string += category
            } else {
                string += "\(category) · "
            }
        }
        return string
    }
}
