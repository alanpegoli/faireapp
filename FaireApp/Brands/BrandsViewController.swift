//
//  BrandsViewController.swift
//  FaireApp
//
//  Created by Alan Henrique Pégoli on 23/03/19.
//  Copyright © 2019 Faire. All rights reserved.
//

import UIKit

class BrandsViewController: UICollectionViewController {
    
    // MARK: - Private Properties
    
    private let refreshControl = UIRefreshControl()
    private let cellIdentifier = "BrandCollectionViewCell"
    private let connectionManager = ConnectionManager()
    private var brands: [Brand] = []
    
    // MARK: - Internal Properties
    
    var category: Category? {
        didSet {
            title = category?.name
        }
    }
    
    // MARK: - Overrided Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib.init(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        refreshControl.addTarget(self, action: #selector(getBrands), for: .valueChanged)
        collectionView.addSubview(refreshControl)
        collectionView.alwaysBounceVertical = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if brands.isEmpty { getBrands() }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return brands.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
        guard let brandCell = cell as? BrandCollectionViewCell else { return cell }
        let brand = brands[indexPath.item]
        brandCell.setUp(with: brand)
        return brandCell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let brand = brands[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ProductsViewController")
        guard let productsViewController = viewController as? ProductsViewController else { return }
        productsViewController.brandToken = brand.token
        navigationController?.pushViewController(productsViewController, animated: true)
    }
    
    // MARK: - Private Methods
    
    @objc private func getBrands() {
        guard let category = category else { return }
        refreshControl.beginRefreshing()
        connectionManager.searchMakers(by: category) { [weak self] (result) in
            guard let strongSelf = self else { return }
            switch result {
            case let .success(brands):
                strongSelf.brands = brands
                DispatchQueue.main.async {
                    strongSelf.collectionView.reloadData()
                }
            case let .error(error):
                strongSelf.showError(error)
            }
            DispatchQueue.main.async {
                strongSelf.refreshControl.endRefreshing()
            }
        }
    }
    
    private func showError(_ error: Error) {
        let alert = UIAlertController(title: "Something Went Wrong", message: error.localizedDescription, preferredStyle: .alert)
        let retry = UIAlertAction(title: "Retry", style: .default, handler: { _ in
            self.getBrands()
        })
        alert.addAction(retry)
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension BrandsViewController: UICollectionViewDelegateFlowLayout  {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberOfCellsForRow = 2
        let spaceInBetween: CGFloat = 16 * (CGFloat(numberOfCellsForRow) + 1)
        let width = (collectionView.bounds.width-spaceInBetween)/CGFloat(numberOfCellsForRow)
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
}
