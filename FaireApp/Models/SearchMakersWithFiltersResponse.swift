//
//  SearchMakersWithFiltersResponse.swift
//  FaireApp
//
//  Created by Alan Henrique Pégoli on 23/03/19.
//  Copyright © 2019 Faire. All rights reserved.
//

import Foundation

struct SearchMakersWithFiltersResponse: Codable {
    var brands: [Brand]
}
