//
//  ViewController.swift
//  FaireApp
//
//  Created by Alan Henrique Pégoli on 21/03/19.
//  Copyright © 2019 Faire. All rights reserved.
//

import UIKit

class CategoriesViewController: UITableViewController {
    
    // MARK: - Private Properties
    
    private let cellIdentifier = "CategoryTableViewCell"
    private let connectionManager = ConnectionManager()
    private var featuredCategories: [Category] = []
    private var productBasedCategories: [Category] = []
    
    // MARK: - Internal Propeties
    
    var categories: [Category] = [] {
        didSet {
            guard allowsRequest else { return }
            featuredCategories = categories.filter { $0.isCategoryFeatured }
            productBasedCategories = categories.filter { !$0.isCategoryFeatured }
        }
    }
    var allowsRequest = true

    // MARK: - Overrided Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = allowsRequest ? UIRefreshControl() : nil
        refreshControl?.addTarget(self, action: #selector(getCategories), for: .valueChanged)
        tableView.register(CategoryTableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.backgroundColor = #colorLiteral(red: 0.9625374675, green: 0.9625598788, blue: 0.9625478387, alpha: 1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getCategories()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return allowsRequest ? 2 : 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allowsRequest {
            return section == 0 ? featuredCategories.count : productBasedCategories.count
        }
        return categories.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if allowsRequest {
            return section == 0 ? "Featured" : "Categories"
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        guard let categoryCell = cell as? CategoryTableViewCell else { return cell }
        let category = getCategory(at: indexPath)
        categoryCell.setUp(with: category)
        return categoryCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = getCategory(at: indexPath)
        if category.subCategories.isEmpty {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "BrandsViewController")
            guard let brandsViewController = viewController as? BrandsViewController else { return }
            brandsViewController.category = category
            navigationController?.pushViewController(brandsViewController, animated: true)
        } else {
            let categoriesViewController = CategoriesViewController(style: .grouped)
            categoriesViewController.allowsRequest = false
            categoriesViewController.title = category.name
            categoriesViewController.categories = category.subCategories
            navigationController?.pushViewController(categoriesViewController, animated: true)
        }
    }
    
    // MARK: - Private Methods
    
    private func getCategory(at indexPath: IndexPath) -> Category {
        if allowsRequest {
            return indexPath.section == 0 ? featuredCategories[indexPath.row] : productBasedCategories[indexPath.row]
        }
        return categories[indexPath.row]
    }
    
    @objc private func getCategories() {
        guard allowsRequest else { return }
        refreshControl?.beginRefreshing()
        connectionManager.getCategories { [weak self] (result) in
            guard let strongSelf = self else { return }
            switch result {
            case let .success(categories):
                strongSelf.categories = categories
                DispatchQueue.main.async {
                    strongSelf.tableView.reloadData()
                }
            case let .error(error):
                strongSelf.showError(error)
            }
            DispatchQueue.main.async {
                strongSelf.refreshControl?.endRefreshing()
            }
        }
    }
    
    private func showError(_ error: Error) {
        let alert = UIAlertController(title: "Something Went Wrong", message: error.localizedDescription, preferredStyle: .alert)
        let retry = UIAlertAction(title: "Retry", style: .default, handler: { _ in
            self.getCategories()
        })
        alert.addAction(retry)
        present(alert, animated: true, completion: nil)
    }
}

