//
//  CategoryTableViewCell.swift
//  FaireApp
//
//  Created by Alan Henrique Pégoli on 21/03/19.
//  Copyright © 2019 Faire. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    
    func setUp(with category: Category) {
        textLabel?.text = category.name
        imageView?.image = category.isCategoryFeatured ? #imageLiteral(resourceName: "featured") : nil
        accessoryType = .disclosureIndicator
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        textLabel?.text = nil
        imageView?.image = nil
        accessoryType = .none
    }
}
