//
//  SearchMakersWithFiltersRequest.swift
//  FaireApp
//
//  Created by Alan Henrique Pégoli on 23/03/19.
//  Copyright © 2019 Faire. All rights reserved.
//

import Foundation

struct SearchMakersWithFiltersRequest: Codable {
    var category: String?
    var paginationData: PaginationData?
    var sortOrder: SortOrder?
    var sortBy: SearchMakersWithFiltersSortBy?
    
    init(category: String?, paginationData: PaginationData?, sortOrder: SortOrder?, sortBy: SearchMakersWithFiltersSortBy?) {
        self.category = category
        self.paginationData = paginationData
        self.sortOrder = sortOrder
        self.sortBy = sortBy
    }
}


