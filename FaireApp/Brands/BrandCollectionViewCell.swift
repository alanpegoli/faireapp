//
//  BrandCollectionViewCell.swift
//  FaireApp
//
//  Created by Alan Henrique Pégoli on 23/03/19.
//  Copyright © 2019 Faire. All rights reserved.
//

import UIKit
import Kingfisher

class BrandCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var minimumOrderAmountCentsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cardView.layer.cornerRadius = 16
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        nameLabel.text = nil
        minimumOrderAmountCentsLabel.text = nil
    }
    
    func setUp(with brand: Brand) {
        nameLabel.text = brand.name
        minimumOrderAmountCentsLabel.text = brand.minimumOrderAmountCentsString
        guard let urlString = brand.images.first?.url else { return }
        let url = URL(string: urlString)
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: url)
        addDropShadow()
    }
    
    private func addDropShadow() {
        contentView.layer.cornerRadius = 16
        contentView.layer.borderWidth = 1
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true;
        
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowRadius = 32
        layer.shadowOpacity = 0.5
        layer.masksToBounds = false;
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: contentView.layer.cornerRadius).cgPath
    }

}
