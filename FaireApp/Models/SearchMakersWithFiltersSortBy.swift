//
//  SearchMakersWithFiltersSortBy.swift
//  FaireApp
//
//  Created by Alan Henrique Pégoli on 23/03/19.
//  Copyright © 2019 Faire. All rights reserved.
//

import Foundation

enum SearchMakersWithFiltersSortBy: String, Codable {
    case name = "NAME"
    case score = "SCORE"
    case firstActiveAt = "FIRST_ACTIVE_AT"
    case minimum = "MINIMUM"
}
