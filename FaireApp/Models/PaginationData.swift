//
//  PaginationData.swift
//  FaireApp
//
//  Created by Alan Henrique Pégoli on 23/03/19.
//  Copyright © 2019 Faire. All rights reserved.
//

import Foundation

struct PaginationData: Codable {
    var pageNumber: Int?
    var pageSize: Int?
    var pageCount: Int?
    var totalResults: Int?
    
    init(pageNumber: Int?, pageSize: Int?, pageCount: Int?, totalResults: Int?) {
        self.pageNumber = pageNumber
        self.pageSize = pageSize
        self.pageCount = pageCount
        self.totalResults = totalResults
    }
}
